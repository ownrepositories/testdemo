class AppString{
  static String deineJob = "Deine Job website";
  static String buttonText = "Kostenlos Registrieren";
  static String login = "Login";
  static String tab1 = "Arbeitnehmer";
  static String tab2 = "Arbeitgeber";
  static String tab3 = "Temporärbüro";

  static String tabText = "Drei einfache Schritte zu deinem neuen Job";
}