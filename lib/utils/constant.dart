import '../generated/assets.dart';

List<String> tabFirstText = [
  "Erstellen dein Lebenslauf",
  "Erstellen dein\nUnternehmensprofil",
  "Erstellen dein\nUnternehmensprofil"
];

List<String> tabSecondText = [
  "Erstellen dein Lebenslauf",
  "Erstellen ein Jobinserat",
  "Erhalte Vermittlungs-\nangebot von Arbeitgeber"
];
List<String> tabThirdText = [
  "Mit nur einem Klick bewerben",
  "Wähle deinen neuen Mitarbeiter aus",
  "Vermittlung nach Provision oder Stundenlohn"
];
List<String> tabSecondImages = [
  Assets.assetsSliderTwoSecond,
  Assets.assetsSliderOneSecond,
  Assets.assetsSliderThirdSecond
];

List<String> tabThirdImages = [
  Assets.assetsSliderSeconfThird,
  Assets.assetsSliderOneThird,
  Assets.assetsSliderThirdThird
];
