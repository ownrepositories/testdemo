import 'package:flutter/material.dart';

class AppColors{
  static Color white = const Color(0xFFFFFFFF);
  static Color grey = const Color(0xFF4A5568);
  static Color lightFrey = const Color(0xFFF7FAFC);
  static Color greyWith200 = const Color(0xFF718096);
  static Color lightGreen = const Color(0xFFE6FFFA);
  static Color green200 = const Color(0xFFEBF4FF);
  static Color darkGrey = const Color(0xFF2D3748);
  static Color grey700 = const Color(0xFFCBD5E0);
  static Color green300 = const Color(0xFF81E6D9);
  static Color darkGreen = const Color(0xFF319795);
  static Color blue = const Color(0xFF3182CE);
  static Color deepGrey = const Color(0xFF707070);
}