import 'package:deine_test/dashboard_view/view/responsive_view/dashboard_mobile.dart';
import 'package:deine_test/dashboard_view/view/responsive_view/dashboard_web.dart';
import 'package:deine_test/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class DashboardView extends StatefulWidget {
  const DashboardView({super.key});

  @override
  State<DashboardView> createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      body: ResponsiveBuilder(
        builder: (context, sizingInformation) {
          if (sizingInformation.deviceScreenType == DeviceScreenType.desktop) {
            return const DashboardWebView();
          }

          if (sizingInformation.deviceScreenType == DeviceScreenType.tablet) {
            return const DashboardMobileView();
          }

          if (sizingInformation.deviceScreenType == DeviceScreenType.mobile) {
            return const DashboardMobileView();
          }
          return const SizedBox.shrink();
        },
      ),
    );
  }
}
