import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../generated/assets.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/app_strings.dart';

class DashBoardComponent {
  static gradiantTopScroll() {
    return Container(
      height: 4,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.centerLeft, end: Alignment.centerRight, colors: [AppColors.darkGreen, AppColors.blue])),
    );
  }

  static stakeLoginView() {
    return Material(
      elevation: 5,
      borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(12), bottomRight: Radius.circular(12)),
      child: Container(
        height: 55,
        width: double.infinity,
        decoration: BoxDecoration(
            color: AppColors.white,
            borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(12), bottomRight: Radius.circular(12))),
        child: Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: Text(
                AppString.login,
                style: GoogleFonts.lato(fontSize: 14, color: AppColors.darkGreen, fontWeight: FontWeight.w600),
              ),
            )),
      ),
    );
  }

  static jobText({double? fontSize, TextAlign? align}) {
    return Text(
      AppString.deineJob,
      textAlign: align ?? TextAlign.left,
      style: GoogleFonts.lato(
          fontSize: fontSize ?? 35,
          color: AppColors.darkGrey,
          fontWeight: FontWeight.bold,
          height: 1.2,
          letterSpacing: 1.26),
    );
  }

  static logInButton({double? horizontalPadding, double? verticalPadding}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: horizontalPadding ?? 10, vertical: verticalPadding ?? 15),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          gradient: LinearGradient(
              begin: Alignment.centerLeft, end: Alignment.centerRight, colors: [AppColors.darkGreen, AppColors.blue])),
      child: Center(
        child: Text(
          AppString.buttonText,
          style: GoogleFonts.lato(fontSize: 14, color: AppColors.lightGreen, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  static topLogo({double? height, double? width}) {
    return Container(
      height: height ?? 50,
      width: width ?? 50,
      decoration: BoxDecoration(
          color: AppColors.white,
          shape: BoxShape.circle,
          image: const DecorationImage(image: AssetImage(Assets.assetsDashboardTop))),
    );
  }

  static tabText({double? fontSize}) {
    return Text(
      AppString.tabText, maxLines: 2,textAlign:TextAlign.center,style: GoogleFonts.lato(fontSize: fontSize ?? 21, color: AppColors.grey, fontWeight: FontWeight.bold),);
  }

  static text1({double? fontSize,required String text}) {
    return Text(
      text, textAlign:TextAlign.center,style: GoogleFonts.lato(fontSize: fontSize ?? 130, color: AppColors.greyWith200, ),);
  }

  static textDes({double? fontSize,required String text}) {
    return Text(
      text,overflow: TextOverflow.visible, maxLines: 3,textAlign:TextAlign.left,style: GoogleFonts.lato(fontSize: fontSize ?? 16, color: AppColors.greyWith200, ),);
  }

}

spaceHeightWidget(double h) {
  return SizedBox(height: h);
}

spaceWidthWidget(double w) {
  return SizedBox(width: w);
}
