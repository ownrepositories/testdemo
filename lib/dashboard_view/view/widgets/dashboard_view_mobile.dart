import 'package:deine_test/dashboard_view/view/widgets/tabbar_view.dart';
import 'package:flutter/material.dart';

import '../../../generated/assets.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/constant.dart';
import 'app_common_component.dart';

class DashboardTopViewMobile extends StatefulWidget {
  const DashboardTopViewMobile({super.key});

  @override
  State<DashboardTopViewMobile> createState() => _DashboardTopViewMobileState();
}

class _DashboardTopViewMobileState extends State<DashboardTopViewMobile> {
  int initialIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      width: double.infinity,
      child: Column(
        children: [
          TabBarViewWidget(
            onCallBack: (value) {
              setState(() {
                initialIndex = value;
              });
            },
          ),
          spaceHeightWidget(30.0),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 80),
            child: DashBoardComponent.tabText(),
          ),
          spaceHeightWidget(20),
          SizedBox(
            width: double.infinity,
            height: 250,
            child: Stack(
              children: [
                Positioned(
                  left: -30,
                  bottom: 0,
                  child: Container(
                    height: 220,
                    width: 220,
                    decoration: BoxDecoration(shape: BoxShape.circle, color: AppColors.lightFrey),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      DashBoardComponent.text1(text: "1."),
                      spaceWidthWidget(20),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Image.asset(
                            Assets.assetsSliderOneFirst,
                            height: 140,
                          ),
                          const Spacer(),
                          DashBoardComponent.textDes(
                            text: tabFirstText[initialIndex],
                          ),
                          spaceHeightWidget(40),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Stack(
            children: [
              Image.asset(
                Assets.assetsCenterBackMobile,
                width: double.infinity,
                fit: BoxFit.fitWidth,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 50.0, top: 30),
                child: Column(
                  children: [
                    SizedBox(
                      height: 150,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          DashBoardComponent.text1(text: "2."),
                          spaceWidthWidget(20),
                          Padding(
                              padding: const EdgeInsets.only(bottom: 3),
                              child: DashBoardComponent.textDes(
                                text: tabSecondText[initialIndex],
                              ))
                        ],
                      ),
                    ),
                    spaceHeightWidget(initialIndex == 1 ? 0 : 30),
                    Image.asset(
                      tabSecondImages[initialIndex],
                      height: initialIndex == 1 ? 180 : 140,
                    )
                  ],
                ),
              )
            ],
          ),
          SizedBox(
            width: double.infinity,
            child: Stack(
              children: [
                Positioned(
                  left: -50,
                  top: 0,
                  child: Container(
                    height: 280,
                    width: 280,
                    decoration: BoxDecoration(shape: BoxShape.circle, color: AppColors.lightFrey),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 80.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          DashBoardComponent.text1(text: "3."),
                          spaceWidthWidget(20),
                          Expanded(
                            child: DashBoardComponent.textDes(
                              text: tabThirdText[initialIndex],
                            ),
                          ),
                          spaceWidthWidget(5),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 40),
                        child: Image.asset(
                          tabThirdImages[initialIndex],
                        ),
                      )
                    ],
                  ),
                ),
                spaceHeightWidget(550)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
