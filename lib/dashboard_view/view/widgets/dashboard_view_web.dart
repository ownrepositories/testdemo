import 'package:deine_test/dashboard_view/view/widgets/app_common_component.dart';
import 'package:deine_test/dashboard_view/view/widgets/tabbar_view.dart';
import 'package:flutter/material.dart';

import '../../../generated/assets.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/constant.dart';

class DashboardTabView extends StatefulWidget {
  const DashboardTabView({super.key});

  @override
  State<DashboardTabView> createState() => _DashboardTabViewState();
}

class _DashboardTabViewState extends State<DashboardTabView> {
  int initialIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TabBarViewWidget(
          onCallBack: (value) {
            setState(() {
              initialIndex = value;
            });
          },
        ),
        spaceHeightWidget(50),
        SizedBox(
          width: 360,
          child: DashBoardComponent.tabText(fontSize: 36),
        ),
        spaceHeightWidget(50),
        Stack(
          children: [
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Stack(
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.width * 0.08,
                          width: MediaQuery.of(context).size.width * 0.08,
                          decoration: BoxDecoration(shape: BoxShape.circle, color: AppColors.lightFrey),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 30.0, top: 20),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              DashBoardComponent.text1(fontSize: MediaQuery.of(context).size.width * 0.05,text: "1."),
                              spaceWidthWidget(20),
                              Padding(
                                  padding: const EdgeInsets.only(bottom: 30),
                                  child: DashBoardComponent.textDes(
                                      text: tabFirstText[initialIndex],
                                      fontSize: MediaQuery.of(context).size.width * 0.012))
                            ],
                          ),
                        )
                      ],
                    ),
                    spaceWidthWidget(50),
                    Image.asset(
                      Assets.assetsSliderOneFirst,
                      height: MediaQuery.of(context).size.width * 0.1,
                    ),
                    spaceWidthWidget(150),
                  ],
                ),
                spaceHeightWidget(20),
                Stack(
                  children: [
                    Image.asset(
                      Assets.assetsCenterBack,
                      width: double.infinity,
                      fit: BoxFit.fitWidth,
                    ),
                    Positioned(
                      bottom: MediaQuery.of(context).size.width * 0.1,
                      left: 0,
                      right: 0,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 120.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              tabSecondImages[initialIndex],
                              height: MediaQuery.of(context).size.width * 0.1,
                            ),
                            spaceWidthWidget(MediaQuery.of(context).size.width * 0.06),
                            DashBoardComponent.text1(fontSize: MediaQuery.of(context).size.width * 0.05,text: "2."),
                            spaceWidthWidget(20),
                            Padding(
                                padding: const EdgeInsets.only(bottom: 30),
                                child: DashBoardComponent.textDes(
                                    text: tabSecondText[initialIndex],
                                    fontSize: MediaQuery.of(context).size.width * 0.012))
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                spaceHeightWidget(20),
                Padding(
                  padding: const EdgeInsets.only(left: 100.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Stack(
                        children: [
                          Container(
                            height: MediaQuery.of(context).size.width * 0.09,
                            width: MediaQuery.of(context).size.width * 0.09,
                            decoration: BoxDecoration(shape: BoxShape.circle, color: AppColors.lightFrey),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 50.0, bottom: 50),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                DashBoardComponent.text1(fontSize: MediaQuery.of(context).size.width * 0.045,text: "3."),
                                spaceWidthWidget(20),
                                Padding(
                                    padding: const EdgeInsets.only(bottom: 30),
                                    child: DashBoardComponent.textDes(
                                        text: tabThirdText[initialIndex],
                                        fontSize: MediaQuery.of(context).size.width * 0.012))
                              ],
                            ),
                          )
                        ],
                      ),
                      spaceWidthWidget(20),
                      Image.asset(
                        tabThirdImages[initialIndex],
                        height: MediaQuery.of(context).size.width * 0.12,
                      ),
                      spaceWidthWidget(150),
                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(
                  right: MediaQuery.of(context).size.width * 0.13, top: MediaQuery.of(context).size.width * 0.087),
              child: Align(
                  alignment: Alignment.center,
                  child: Image.asset(
                    Assets.assetsArrowOne,
                    height: MediaQuery.of(context).size.width * 0.15,
                  )),
            ),
            Padding(
              padding: EdgeInsets.only(
                  right: MediaQuery.of(context).size.width * 0.07, top: MediaQuery.of(context).size.width * 0.32),
              child: Align(
                  alignment: Alignment.center,
                  child: Image.asset(
                    Assets.assetsArrowTwo,
                    height: MediaQuery.of(context).size.width * 0.12,
                  )),
            ),
          ],
        ),
        spaceHeightWidget(800)
      ],
    );
  }
}
