import 'package:deine_test/generated/assets.dart';
import 'package:flutter/material.dart';
import 'app_common_component.dart';

class DashboardTopView extends StatefulWidget {
  const DashboardTopView({super.key});

  @override
  State<DashboardTopView> createState() => _DashboardTopViewState();
}

class _DashboardTopViewState extends State<DashboardTopView> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        DashBoardComponent.gradiantTopScroll(),
        Stack(
          children: [
            Image.asset(
              Assets.assetsTopBack,
              width: double.infinity,
              fit: BoxFit.fitWidth,
            ),
            DashBoardComponent.stakeLoginView(),
            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.07),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        DashBoardComponent.jobText(fontSize: MediaQuery.of(context).size.width * 0.03),
                       spaceHeightWidget(50),
                        DashBoardComponent.logInButton(
                            horizontalPadding: MediaQuery.of(context).size.width * 0.05,
                            verticalPadding: MediaQuery.of(context).size.width * 0.005),
                      ],
                    ),
                  ),
                 spaceWidthWidget(50),
                  DashBoardComponent.topLogo(
                      height: MediaQuery.of(context).size.width * 0.2, width: MediaQuery.of(context).size.width * 0.2),
                  spaceWidthWidget( MediaQuery.of(context).size.width * 0.15)
                ],
              ),
            )
          ],
        )
      ],
    );
  }
}
