import 'package:deine_test/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:scrollview_observer/scrollview_observer.dart';

import '../../../utils/app_colors.dart';

class TabBarViewWidget extends StatefulWidget {
  final Function(int value) onCallBack;

  const TabBarViewWidget({super.key, required this.onCallBack});

  @override
  State<TabBarViewWidget> createState() => _TabBarViewWidgetState();
}

class _TabBarViewWidgetState extends State<TabBarViewWidget> {
  ScrollController controller = ScrollController();
  ListObserverController observerController = ListObserverController();
  List tabs = [AppString.tab1, AppString.tab2, AppString.tab3];
  int containerIndex = 0;

  @override
  void initState() {
    observerController = ListObserverController(controller: controller);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 455,
      height: 40,
      child: Container(
        decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(12), border: Border.all(color: AppColors.grey700)),
        child: ListViewObserver(
          controller: observerController,
          child: ListView.separated(
              controller: controller,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return Theme(
                  data: Theme.of(context).copyWith(
                      splashColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      splashFactory: NoSplash.splashFactory),
                  child: InkWell(
                    onTap: () {
                      observerController.animateTo(
                        index: index,
                        duration: const Duration(milliseconds: 250),
                        curve: Curves.ease,
                      );
                      setState(() {
                        containerIndex = index;
                      });
                      widget.onCallBack(index);
                    },
                    child: AnimatedContainer(
                      decoration: BoxDecoration(
                          color: containerIndex == index ? AppColors.green300 : Colors.transparent,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(index == 0 ? 10 : 0),
                              topLeft: Radius.circular(index == 0 ? 10 : 0),
                              bottomRight: Radius.circular(index == 2 ? 10 : 0),
                              topRight: Radius.circular(index == 2 ? 10 : 0))),
                      width: 150,
                      duration: const Duration(milliseconds: 300),
                      curve: Curves.fastOutSlowIn,
                      child: Center(
                          child: Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Text(
                          tabs[index],
                          overflow: TextOverflow.ellipsis,
                          style: GoogleFonts.lato(
                              fontSize: 14,
                              color: containerIndex == index ? AppColors.white : AppColors.darkGreen,
                              fontWeight: FontWeight.w600,
                              letterSpacing: 0.84),
                        ),
                      )),
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Container(
                  height: 35,
                  width: 2,
                  color: AppColors.grey700,
                );
              },
              itemCount: 3),
        ),
      ),
    );
  }
}
