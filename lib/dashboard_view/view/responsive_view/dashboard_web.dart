import 'package:deine_test/dashboard_view/view/widgets/app_common_component.dart';
import 'package:flutter/material.dart';

import '../widgets/dashboard_top_view_web.dart';
import '../widgets/dashboard_view_web.dart';

class DashboardWebView extends StatefulWidget {
  const DashboardWebView({super.key});

  @override
  State<DashboardWebView> createState() => _DashboardWebViewState();
}

class _DashboardWebViewState extends State<DashboardWebView> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const DashboardTopView(),
          spaceHeightWidget(20),
          const DashboardTabView()
        ],
      ),
    );
  }
}
