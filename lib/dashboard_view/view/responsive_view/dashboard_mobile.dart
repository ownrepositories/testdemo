import 'package:deine_test/utils/app_colors.dart';
import 'package:flutter/material.dart';

import '../../../generated/assets.dart';
import '../widgets/app_common_component.dart';
import '../widgets/dashboard_view_mobile.dart';

class DashboardMobileView extends StatefulWidget {
  const DashboardMobileView({super.key});

  @override
  State<DashboardMobileView> createState() => _DashboardMobileViewState();
}

class _DashboardMobileViewState extends State<DashboardMobileView> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.95,
            width: double.infinity,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [AppColors.green200, AppColors.lightGreen])),
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: [
                      spaceHeightWidget(59.0),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 30),
                        child: DashBoardComponent.jobText(
                            fontSize: MediaQuery.of(context).size.width * 0.10, align: TextAlign.center),
                      ),
                      spaceHeightWidget(15.0),
                      const Image(image: AssetImage(Assets.assetsDashboardTop)),
                      const Image(
                        image: AssetImage(Assets.assetsBackMobile),
                        width: double.infinity,
                        fit: BoxFit.fitWidth,
                      ),
                      const DashboardTopViewMobile(),
                    ],
                  ),
                ),
                Column(
                  children: [
                    DashBoardComponent.gradiantTopScroll(),
                    DashBoardComponent.stakeLoginView(),
                    const Spacer(),
                    bottomButtonView()
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget bottomButtonView() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            height: 50,
            decoration: BoxDecoration(
              color: AppColors.grey700,
              borderRadius: const BorderRadius.only(topRight: Radius.circular(12), topLeft: Radius.circular(12)),
            ),
          ),
          Container(
            width: double.infinity,
            height: 100,
            margin: const EdgeInsets.only(top: 2),
            decoration: BoxDecoration(
              color: AppColors.white,
              borderRadius: const BorderRadius.only(topRight: Radius.circular(12), topLeft: Radius.circular(12)),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 25),
              child: DashBoardComponent.logInButton(
                  horizontalPadding: MediaQuery.of(context).size.width * 0.10,
                  verticalPadding: MediaQuery.of(context).size.width * 0.005),
            ),
          ),
        ],
      ),
    );
  }
}
