# deine_test

A new Flutter project.

## Getting Started
## flutter release version 3.10.5

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


### use command run this app in web :
```dart
flutter run -d chrome --web-renderer html
```
``` for auto generate files
flutter pub run build_runner build --drelete-conflicting-outputs
```

### To generate build use below command
```dart b
flutter build web --web-renderer html --release
```


